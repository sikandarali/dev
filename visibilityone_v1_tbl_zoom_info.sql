-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: db-visionpoint.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_zoom_info`
--

DROP TABLE IF EXISTS `tbl_zoom_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_zoom_info` (
  `zoom_id` int unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int unsigned DEFAULT NULL,
  `APIKey` varchar(255) DEFAULT NULL,
  `APISeceret` varchar(255) DEFAULT NULL,
  `Token` varchar(255) DEFAULT NULL,
  `active` int DEFAULT '1',
  `last_error_stamp` datetime DEFAULT NULL,
  `error_message` varchar(255) DEFAULT NULL,
  `notification` int DEFAULT '1',
  `web_hook_token_verification` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`zoom_id`),
  KEY `idx_company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_zoom_info`
--

LOCK TABLES `tbl_zoom_info` WRITE;
/*!40000 ALTER TABLE `tbl_zoom_info` DISABLE KEYS */;
INSERT INTO `tbl_zoom_info` VALUES (41,395,'LXBeso3zTqyJfpxlNpO9Bw','AHthEIHc7cLyhfJZh84gZdWQGokboWJp851p',NULL,1,NULL,'',1,'k1q19fWgTwW0hx0QF4IG0A'),(42,400,'c4i_DpnKR2OqUjg-q9Upbg','xAH9Y0q2MePItVNg5AYrcrn41Da6lRDFYDD9',NULL,1,NULL,NULL,0,'SElUjR4LRgqh0UNMwYdHRQ'),(43,401,'twHRSqRfRxS7jVF2E_10Ww','hq8QLDZovxmC1JhWEjOohuIbwKwwOncWs89s',NULL,1,NULL,'',1,'mhoDFet8S7mBLEsrIEgySQ'),(44,408,'wLv5HijWSdO-HlwDM9SJ8g','ndXhyzh8gKJZxp2qx1JPKtZXcEq8n0mYWuAx',NULL,1,NULL,NULL,1,'xW50dYgwRBm4ggrCD5AdbA'),(45,402,'iyeFrq38SdGCAJt9wm9_mg','4ODjp5gfWZGAuNvmRn4mDlwK42cuuaf3pIyW',NULL,1,NULL,NULL,1,'Uo0nNF_8Qg-b3aU1C-0atQ'),(46,410,'b1oPP3NNR_3BG69Bhl3Jw','dOVjyT6mDaY25eQEUwjBLXL3w1ukdKaO',NULL,1,NULL,'',1,'jH_vVG2aQPi_gpvP-ESPZQ'),(47,412,'RvECNw7sRwaiCTKuLCGdqg','7bypIRE9I2UOFR8ouu29nVVQraOaesCCjjyU',NULL,1,NULL,'',1,'hDK0F4kLSRukwkheV_Y-QA'),(48,398,'DIeNteRRSluFB_gE0bIUiw','T3tGcUXAz5GDaDbhShdiwM32l7gFqFIpr6ae',NULL,0,NULL,NULL,0,'V7rnhfkgSoKGOGcvdVS0Ag');
/*!40000 ALTER TABLE `tbl_zoom_info` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-14  4:23:30
