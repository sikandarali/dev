-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: db-visionpoint.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_plugin_version`
--

DROP TABLE IF EXISTS `tbl_plugin_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_plugin_version` (
  `plugin_version_id` int NOT NULL AUTO_INCREMENT,
  `version` varchar(127) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `stamp` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int DEFAULT '0',
  `file_size` varchar(50) DEFAULT NULL,
  `release_notes` longtext,
  `email_notes` longtext,
  `update_schedule` datetime DEFAULT NULL,
  `update_status` enum('OPEN','SCHEDULED','UPDATING','COMPLETED','FAILED') DEFAULT NULL,
  `include_release_notes` tinyint DEFAULT '0',
  PRIMARY KEY (`plugin_version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_plugin_version`
--

LOCK TABLES `tbl_plugin_version` WRITE;
/*!40000 ALTER TABLE `tbl_plugin_version` DISABLE KEYS */;
INSERT INTO `tbl_plugin_version` VALUES (23,'1.0.45.0','UnifyPlusPluginSetup-1.0.45.0.exe','2021-11-15 01:54:06',0,'22MB','Disabled Collector api Server',NULL,NULL,'COMPLETED',0),(24,'1.0.47.0','UnifyPlusPluginSetup-1.0.47.0','2022-03-09 21:54:21',0,'22MB','Removed pathsolution calls in app simulator',NULL,NULL,'COMPLETED',0),(25,'1.0.48.0','UnifyPlusPluginSetup-1.0.48.0.exe','2022-03-10 07:58:37',0,'22MB','Added call simulator actions',NULL,NULL,'COMPLETED',0),(26,'1.0.49.0','UnifyPlusPluginSetup-1.0.49.0.exe','2022-03-21 08:23:00',0,'22MB','Added crestron client list',NULL,NULL,'COMPLETED',0),(27,'1.0.50.0','UnifyPlusPluginSetup-1.0.50.0.exe','2022-03-31 11:10:27',0,'22MB','Fixed login error in some PCs due to mac address',NULL,NULL,'COMPLETED',0),(28,'1.0.51.0','UnifyPlusPluginSetup-1.0.51.0.exe','2022-04-07 12:17:53',0,'22MB','Fixed disconnection issue due to cpu info error in WMI',NULL,NULL,'COMPLETED',0),(29,'1.0.52.0','UnifyPlusPluginSetup-1.0.52.0.exe','2022-04-25 17:36:05',0,'22MB','Changed determining of teams room installation',NULL,NULL,'COMPLETED',0),(30,'1.0.55.0','UnifyPlusPluginSetup-1.0.55.0.exe','2022-11-03 10:27:05',0,'23.9MB','Added agreement and bug fixes',NULL,'2022-11-08 13:35:00','COMPLETED',0),(31,'1.0.56.2','UnifyPlusPluginSetup-1.0.56.2.exe','2022-11-09 14:36:05',0,'23.9MB','Bug Fixes',NULL,'2022-11-15 15:40:00','COMPLETED',0),(32,'1.0.58.2','UnifyPlusPluginSetup-1.0.58.0.exe','2022-11-10 00:00:00',0,'28.8MB','Minor Bug Fixes',NULL,NULL,'COMPLETED',0),(35,'1.0.60.0','UnifyPlusPluginSetup-1.0.60.0.exe','2022-11-20 00:00:00',0,'28.8MB','Minor Bug Fixes',NULL,NULL,'COMPLETED',0),(36,'2.0.0.3','UnifyPlusPluginSetup-2.0.0.3.exe','2022-11-26 00:00:00',0,'24.8MB','Minor Bug Fixes','Minor bug fix for MTR Rooms',NULL,'COMPLETED',0),(37,'2.0.0.4','UnifyPlusPluginSetup-2.0.0.4.exe','2023-04-19 00:00:00',0,'24.8MB','Minor Bug Fixes','Minor bug fix for MTR Rooms','2023-05-17 14:45:00','COMPLETED',0),(38,'2.0.0.6','UnifyPlusPluginSetup-2.0.0.6.exe','2023-05-19 00:00:00',1,'24.8MB','Minor Bug Fixes','Minor bug fix for MTR Rooms','2023-08-24 16:50:00','UPDATING',0);
/*!40000 ALTER TABLE `tbl_plugin_version` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-14  4:07:46
