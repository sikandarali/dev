-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: db-visionpoint.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_site`
--

DROP TABLE IF EXISTS `tbl_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site` (
  `site_id` int unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int unsigned DEFAULT NULL,
  `virtual` int DEFAULT '0',
  `parent_id` int unsigned DEFAULT NULL,
  `collector_id` int unsigned DEFAULT NULL,
  `site_name` varchar(127) DEFAULT NULL,
  `address1` varchar(127) DEFAULT NULL,
  `address2` varchar(127) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `zip` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `active` int DEFAULT '1',
  `verified` int DEFAULT '0',
  `contact_id` int unsigned DEFAULT NULL,
  `paused` int DEFAULT '0',
  `status` int DEFAULT '-1',
  `alerts` int DEFAULT '1',
  `qos_paused` int DEFAULT '0',
  `localUTC` varchar(50) DEFAULT NULL,
  `wellness_check_time` time DEFAULT NULL,
  `schedule_interval` int unsigned DEFAULT NULL,
  PRIMARY KEY (`site_id`),
  KEY `company_idx` (`company_id`),
  KEY `report_idx` (`company_id`,`active`,`verified`),
  CONSTRAINT `company_site` FOREIGN KEY (`company_id`) REFERENCES `tbl_company` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site`
--

LOCK TABLES `tbl_site` WRITE;
/*!40000 ALTER TABLE `tbl_site` DISABLE KEYS */;
INSERT INTO `tbl_site` VALUES (28,36,0,NULL,91,'TestBridge','brdge1','brdge2','Cebu','Badakhshan','1','1111','2020-10-06 06:46:54',0,1,445,0,-1,1,0,NULL,NULL,NULL),(29,36,0,NULL,91,'testBridge2nd','add1','add2','ce','al-\\\'Ayun','244','232','2020-10-06 06:54:21',0,1,445,0,-1,1,0,NULL,NULL,NULL),(30,36,0,NULL,92,'Liz-VM','Cebu','Minglanilla','Cebu','Central Visayas','174','6000','2020-10-06 08:16:34',0,1,466,0,-1,1,0,NULL,NULL,NULL),(31,36,0,NULL,93,'Liz-VM','Cebu','Minglanilla','cebu','Central Visayas','174','6000','2020-10-06 08:38:17',0,1,466,0,-1,1,0,NULL,NULL,NULL),(32,36,0,NULL,91,'testBridgeUpdate','add1','add2','ceb','Habiganj','19','123123','2020-10-06 09:29:08',0,1,445,0,-1,1,0,NULL,NULL,NULL),(33,36,0,NULL,91,'testUpdate','addrezz1','addrezz2','cebu','Central Visayas','174','11111','2020-10-07 04:27:34',0,1,445,0,-1,1,0,NULL,NULL,NULL),(34,36,0,NULL,91,'testAxe','addwe1','addwe2','asd','West-Vlaanderen','22','222','2020-10-07 08:51:46',0,1,445,0,-1,1,0,NULL,NULL,NULL),(35,36,0,NULL,91,'testPlugin','add1','add2','cet','\\\'Isa','18','2222','2020-10-07 15:17:15',0,1,445,0,-1,1,0,NULL,NULL,NULL),(36,36,0,NULL,91,'testPlugin2','qwe1','qwe2','asd','Wallonne','22','2222','2020-10-07 15:30:43',0,1,445,0,-1,1,0,NULL,NULL,NULL),(37,36,0,NULL,91,'testPlugin3','qwe1','qwe2','asd','\\\'Ayn Daflah','4','2222','2020-10-07 16:06:19',0,1,445,0,-1,1,0,NULL,NULL,NULL),(38,36,0,NULL,91,'pTestLiz','add1','add2','city','Badgis','1','3333','2020-10-07 16:17:30',0,1,445,0,-1,1,0,NULL,NULL,NULL),(39,36,0,NULL,91,'pTestLiz2','addw1','addw2','cet','Bamingui-Bangoran','42','3333','2020-10-07 17:18:22',0,1,445,0,-1,1,0,NULL,NULL,NULL),(40,36,0,NULL,91,'testSiteAPI','add1A','ADD2','qwewqe','Badakhshan','1','1231','2020-10-07 17:31:59',0,1,445,0,-1,1,0,NULL,NULL,NULL),(41,36,0,NULL,91,'testApi','ad1','a2','asd','Badakhshan','1','222','2020-10-07 17:49:55',0,1,445,0,-1,1,0,NULL,NULL,NULL),(42,36,0,NULL,91,'testTest','we','we','asd','Saint Andrew','20','2222','2020-10-07 18:09:17',0,1,445,0,-1,1,0,NULL,NULL,NULL),(43,36,0,NULL,91,'newVersion','q','q','qwe','Badakhshan','1','222','2020-10-07 18:48:09',0,1,445,0,-1,1,0,NULL,NULL,NULL),(44,36,0,NULL,91,'version2','qwe','qwe','sd','Saint Andrew','20','222','2020-10-07 18:54:40',0,1,445,0,-1,0,0,NULL,NULL,NULL),(46,36,0,NULL,NULL,'Liz-PC','Cebu','Cebu','Cebu','Central Visayas','174','6000','2020-11-04 12:47:17',0,0,466,0,-1,1,0,NULL,NULL,NULL),(52,36,0,NULL,88,'update','ad1','ad2','ceb','Central Visayas','174','123','2020-11-21 02:14:49',0,1,445,0,-1,1,0,NULL,NULL,NULL),(56,36,0,NULL,91,'v1','ad1','ad2','ceb','Central Visayas','174','123123','2020-12-21 11:41:32',0,1,445,0,-1,1,0,NULL,NULL,NULL),(92,36,0,NULL,200,'V1 test','1 San Antonio Ave','','SG','California','233','90280','2022-03-05 00:51:53',0,1,107,0,-1,1,0,NULL,NULL,NULL),(94,36,0,NULL,202,'V1 Dev testing','Cebu','Cebu','Cebu City','Central Visayas','174','6045','2022-03-11 06:37:12',0,1,586,0,-1,1,0,NULL,NULL,NULL),(97,395,1,95,203,'VisionPoint Building B',NULL,NULL,NULL,NULL,NULL,NULL,'2022-03-15 19:08:31',0,1,53978,0,1,1,0,NULL,NULL,NULL),(98,36,1,92,200,'Colorado Virtual',NULL,NULL,NULL,NULL,NULL,NULL,'2022-03-25 03:20:52',0,1,106,0,-1,1,0,NULL,NULL,NULL),(102,36,0,NULL,200,'Lab','123 street','','here','California','233','90210','2022-05-27 04:38:07',0,1,106,0,-1,1,0,NULL,NULL,NULL),(103,36,0,NULL,208,'JT','Test','','Venice','Florida','233','34285-3710','2022-08-03 02:12:27',0,1,589,0,-1,1,0,NULL,NULL,NULL),(104,405,0,NULL,210,'McCarter & English, LLP','185 Asylum St','','Hartford','Connecticut','233','06103','2022-10-28 18:02:39',1,1,54012,0,-1,1,0,NULL,NULL,NULL),(105,395,0,NULL,NULL,'David Dixon PC','152 Rockwell Road, Newington','','Newington','Connecticut','233','06111','2023-03-22 19:05:10',0,0,53975,0,1,1,0,NULL,NULL,NULL),(106,415,0,NULL,214,'Seward & Kissel DC','DC 15LRG Teams Room','','DC','Washington','233','20006','2023-03-23 14:49:18',0,1,54036,0,-1,1,0,NULL,NULL,NULL),(107,415,0,NULL,NULL,'Seward & Kissel NYC','111 Battery Park pl','','NY','New York','233','10011','2023-03-23 14:51:36',0,0,54036,0,-1,1,0,NULL,NULL,NULL),(108,415,0,NULL,215,'Seward & Kissel DC Teams Room','901 K st NW ','','Washington ','Washington','233','20001','2023-03-24 16:20:01',0,1,54036,0,-1,1,0,NULL,NULL,NULL),(109,419,0,NULL,216,'Kleinberg NY','500 5th Ave','','New York','New York','233','10110','2023-03-27 19:27:18',1,1,540378,0,-1,1,0,NULL,NULL,NULL),(111,395,0,NULL,203,'VisionPoint HQ','152 Rockwell Rd','','Newington','Connecticut','233','06111','2023-03-28 19:10:17',1,1,53978,0,1,1,0,NULL,NULL,NULL),(112,395,1,111,203,'Building A',NULL,NULL,NULL,NULL,NULL,NULL,'2023-03-28 19:17:02',1,1,53978,0,1,1,0,NULL,NULL,NULL),(113,395,1,111,203,'Building B',NULL,NULL,NULL,NULL,NULL,NULL,'2023-03-28 19:17:17',1,1,53978,0,1,1,0,NULL,NULL,NULL),(114,36,0,NULL,218,'Recodo','Recodo','Recodo','Recodo','Hidd','18','11111','2023-04-21 07:00:34',0,1,107,0,-1,1,0,'UTC-07:00',NULL,NULL),(115,36,1,114,218,'Sta catalina',NULL,NULL,NULL,NULL,NULL,NULL,'2023-04-21 07:01:44',0,1,428,0,-1,1,0,NULL,NULL,NULL),(116,36,0,NULL,208,'JT','408 Beach Rd','','Venice','Florida','233','34285','2023-05-15 21:02:44',0,1,589,0,-1,1,0,'UTC-04:00',NULL,NULL),(117,36,0,NULL,219,'JT','123 Beach Rd','','Test','Georgia','233','34255','2023-05-18 17:07:11',0,1,589,0,-1,1,0,'UTC-04:00',NULL,NULL),(118,36,0,NULL,218,'Signal Village','Signal Village','Signal Village','Signal Village','Bicol','174','1632','2023-05-29 17:58:59',1,1,106,0,-1,1,0,'UTC-07:00',NULL,NULL),(119,36,0,NULL,219,'JT Home','8 Breach Rd','','gr','Alabama','233','34285','2023-05-31 13:00:39',0,1,589,0,-1,1,0,'UTC-04:00',NULL,NULL),(120,36,0,NULL,219,'JT','hgikjnkj','','dgbfvsd','Guam','233','34285','2023-06-07 14:56:02',0,1,589,0,-1,1,0,'UTC-04:00',NULL,NULL),(121,421,0,NULL,222,'HQ','55 walls Dr','','Fairfield','Connecticut','233','06824','2023-08-21 14:09:42',1,1,540397,0,-1,1,0,'UTC-04:00','00:00:00',NULL),(122,415,0,NULL,223,'SK New York Lobby','One Battery Park Plaza','','New York','New York','233','10011','2023-09-19 15:58:35',1,1,54029,0,-1,1,0,'UTC-04:00','00:00:00',NULL),(123,416,0,NULL,224,'Hartford','242 Trumbull St','','Hartford','Connecticut','233','06103','2023-09-21 16:21:09',1,1,540401,0,-1,1,0,'UTC-07:00','00:00:00',NULL),(124,417,0,NULL,225,'Hackensack','25 Main St.','','Hackensack','New Jersy','233','07601','2023-10-09 23:12:30',1,1,54031,0,-1,1,0,'UTC-07:00','00:00:00',NULL),(125,398,0,NULL,NULL,'NorthRock NYC','437 Madison Ave','','New York','New York','233','10011','2023-11-02 14:44:47',1,0,540403,0,-1,1,0,NULL,'00:00:00',NULL),(126,398,0,NULL,219,'test','111 Beach Rd','','afvffd','Hawaii','233','12345','2023-11-02 18:53:44',1,1,540404,0,-1,1,0,'-04:00','00:00:00',NULL),(127,36,0,NULL,NULL,'Test','pakistan','lahore','lahore','Punjab','167','54000','2023-11-14 12:26:10',1,0,565,0,-1,1,0,NULL,'00:00:00',NULL);
/*!40000 ALTER TABLE `tbl_site` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-14  4:04:29
