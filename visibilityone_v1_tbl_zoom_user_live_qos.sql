-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: db-visionpoint.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_zoom_user_live_qos`
--

DROP TABLE IF EXISTS `tbl_zoom_user_live_qos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_zoom_user_live_qos` (
  `zoom_user_live_qos_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `zoom_user_id` int unsigned DEFAULT NULL,
  `company_id` int unsigned DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `meetingId` bigint DEFAULT NULL,
  `meeting_uuid` varchar(127) DEFAULT NULL,
  `device` varchar(45) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `camera` varchar(255) DEFAULT NULL,
  `speaker` varchar(255) DEFAULT NULL,
  `microphone` varchar(255) DEFAULT NULL,
  `network_type` varchar(45) DEFAULT NULL,
  `connection_type` varchar(45) DEFAULT NULL,
  `recording` int DEFAULT NULL,
  `stamp` datetime DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `total_bitrate` bigint DEFAULT NULL,
  `total_latency` int DEFAULT NULL,
  `total_jitter` int DEFAULT NULL,
  `avg_packet_loss` double DEFAULT NULL,
  `audio_input` json DEFAULT NULL,
  `audio_output` json DEFAULT NULL,
  `video_input` json DEFAULT NULL,
  `video_output` json DEFAULT NULL,
  `as_input` json DEFAULT NULL,
  `as_output` json DEFAULT NULL,
  `frame_rate` int DEFAULT NULL,
  `health` int DEFAULT NULL,
  `health_info` json DEFAULT NULL,
  `qos` int DEFAULT NULL,
  `people_count` int DEFAULT '0',
  PRIMARY KEY (`zoom_user_live_qos_id`)
) ENGINE=InnoDB AUTO_INCREMENT=276777 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_zoom_user_live_qos`
--

LOCK TABLES `tbl_zoom_user_live_qos` WRITE;
/*!40000 ALTER TABLE `tbl_zoom_user_live_qos` DISABLE KEYS */;
INSERT INTO `tbl_zoom_user_live_qos` VALUES (276773,725714,402,'XF8oJnzjT6i6jzxBnQEl_A','Alexander Shvarts',99945378807,'RHUsSC0QQD+LKzpbrkRdVQ==','Windows','149.36.48.72','USB Video Device','Headset Earphone (Sennheiser BTD 800 USB)','Headset Microphone (Sennheiser BTD 800 USB)','Others','UDP',0,'2023-04-17 22:06:51','2023-04-17 22:03:00',987136,296,4,0.2,'{\"jitter\": 1, \"bitrate\": 92160, \"latency\": 72, \"avg_loss\": 0, \"max_loss\": 0}','{\"jitter\": 3, \"bitrate\": 6144, \"latency\": 76, \"avg_loss\": 0, \"max_loss\": 0}','{\"jitter\": 0, \"bitrate\": 779264, \"latency\": 72, \"avg_loss\": 0.2, \"max_loss\": 2.9, \"frame_rate\": 21, \"resolution\": \"640*360\"}','{\"jitter\": 0, \"bitrate\": 109568, \"latency\": 76, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 26, \"resolution\": \"640*360\"}','{\"jitter\": 0, \"bitrate\": 0, \"latency\": 0, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 0, \"resolution\": \"\"}','{\"jitter\": 0, \"bitrate\": 0, \"latency\": 0, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 0, \"resolution\": \"\"}',47,NULL,NULL,1,0),(276774,725640,402,'cwcXOI85T_-V6h0Yudpt7w','Greg Lundell',91818078980,'2n7kkLh0SfOvretr6Vb4ag==','Windows','12.53.4.251','Logi C615 HD WebCam','Speakers (Dell AC511 USB SoundBar)','Microphone (Logi C615 HD WebCam)','Wired','UDP',0,'2023-04-17 22:06:51','2023-04-17 21:06:00',626688,320,7,0,'{\"jitter\": 2, \"bitrate\": 151552, \"latency\": 63, \"avg_loss\": 0, \"max_loss\": 0}','{\"jitter\": 2, \"bitrate\": 29696, \"latency\": 67, \"avg_loss\": 0, \"max_loss\": 0}','{\"jitter\": 0, \"bitrate\": 257024, \"latency\": 61, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 11, \"resolution\": \"320*180\"}','{\"jitter\": 2, \"bitrate\": 94208, \"latency\": 67, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 11, \"resolution\": \"160*90\"}','{\"jitter\": 1, \"bitrate\": 94208, \"latency\": 62, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 14, \"resolution\": \"1920*1080\"}','{\"jitter\": 0, \"bitrate\": 0, \"latency\": 0, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 0, \"resolution\": \"\"}',36,NULL,NULL,-1,0),(276775,725705,402,'uTyonnVhR8yYqHWwKlm_Ug','Giacomo Di Liberto',99945378807,'RHUsSC0QQD+LKzpbrkRdVQ==','Windows','67.245.227.35','Integrated Webcam','Headset (AirPods Pro Hands-Free AG Audio)','Headset (AirPods Pro Hands-Free AG Audio)','Wifi','UDP',0,'2023-04-17 22:06:51','2023-04-17 22:03:00',1197056,78,10,0,'{\"jitter\": 2, \"bitrate\": 24576, \"latency\": 17, \"avg_loss\": 0, \"max_loss\": 0}','{\"jitter\": 5, \"bitrate\": 34816, \"latency\": 20, \"avg_loss\": 0, \"max_loss\": 0}','{\"jitter\": 0, \"bitrate\": 501760, \"latency\": 19, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 26, \"resolution\": \"640*360\"}','{\"jitter\": 3, \"bitrate\": 635904, \"latency\": 22, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 23, \"resolution\": \"640*360\"}','{\"jitter\": 0, \"bitrate\": 0, \"latency\": 0, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 0, \"resolution\": \"\"}','{\"jitter\": 0, \"bitrate\": 0, \"latency\": 0, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 0, \"resolution\": \"\"}',49,NULL,NULL,1,0),(276776,923304,402,'MG_PvGKxRoKO5l3v0Sk5Kw','New York Conference Room A',91818078980,'2n7kkLh0SfOvretr6Vb4ag==','Zoom Rooms','144.121.77.154','Yealink A30 Camera','Yealink A30 Audio','Yealink A30 Audio','Wired','UDP',0,'2023-04-17 22:06:51','2023-04-17 21:06:00',625664,15,33,0,'{\"jitter\": 12, \"bitrate\": 27648, \"latency\": 0, \"avg_loss\": 0, \"max_loss\": 1.6}','{\"jitter\": 8, \"bitrate\": 154624, \"latency\": 11, \"avg_loss\": 0, \"max_loss\": 3.6}','{\"jitter\": 8, \"bitrate\": 95232, \"latency\": 0, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 11, \"resolution\": \"160*90\"}','{\"jitter\": 1, \"bitrate\": 254976, \"latency\": 2, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 11, \"resolution\": \"320*180\"}','{\"jitter\": 0, \"bitrate\": 0, \"latency\": 0, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 0, \"resolution\": \"\"}','{\"jitter\": 4, \"bitrate\": 93184, \"latency\": 2, \"avg_loss\": 0, \"max_loss\": 0, \"frame_rate\": 14, \"resolution\": \"1920*1080\"}',36,NULL,NULL,-1,0);
/*!40000 ALTER TABLE `tbl_zoom_user_live_qos` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-14  4:23:39
