-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: db-visionpoint.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `xnx_logs`
--

DROP TABLE IF EXISTS `xnx_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `xnx_logs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `key` tinytext NOT NULL,
  `value` longtext NOT NULL,
  `data` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xnx_logs`
--

LOCK TABLES `xnx_logs` WRITE;
/*!40000 ALTER TABLE `xnx_logs` DISABLE KEYS */;
INSERT INTO `xnx_logs` VALUES (1,'nping','{\"Avgrtt\":\"6182.526\",\"IP\":\"10.100.200.12\",\"LostNum\":\"-12\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"17010.000\",\"Minrtt\":\"2.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"17\"}','2023-10-25 17:41:32'),(2,'nping','{\"Avgrtt\":\"7767.185\",\"IP\":\"10.5.0.40\",\"LostNum\":\"-11\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"20036.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"16\"}','2023-10-25 17:41:33'),(3,'nping_caught','{\"Avgrtt\":\"302442.361\",\"IP\":\"10.5.0.45\",\"LostNum\":\"-3\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"801429.300\",\"Minrtt\":\"1007.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"8\"}','2023-10-25 17:43:24'),(4,'nping_caught','{\"Avgrtt\":\"3939.712\",\"IP\":\"172.29.40.111\",\"LostNum\":\"-27\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"8000.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"32\"}','2023-10-25 17:44:19'),(5,'nping_caught','{\"Avgrtt\":\"5535.663\",\"IP\":\"10.100.200.12\",\"LostNum\":\"-10\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"16022.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"15\"}','2023-10-25 17:45:07'),(6,'nping_caught','{\"Avgrtt\":\"62399.015\",\"IP\":\"10.5.0.41\",\"LostNum\":\"-11\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"906304.300\",\"Minrtt\":\"2004.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"16\"}','2023-10-25 17:45:12'),(7,'nping_caught','{\"Avgrtt\":\"3393.962\",\"IP\":\"172.29.40.111\",\"LostNum\":\"-28\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"8000.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"33\"}','2023-10-25 17:45:19'),(8,'nping_caught','{\"Avgrtt\":\"11882.349\",\"IP\":\"172.21.100.73\",\"LostNum\":\"-12\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"21000.000\",\"Minrtt\":\"6000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"17\"}','2023-10-25 17:45:30'),(9,'nping_caught','{\"Avgrtt\":\"3700.690\",\"IP\":\"172.29.40.111\",\"LostNum\":\"-28\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"8000.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"33\"}','2023-10-25 17:46:19'),(10,'nping_caught','{\"Avgrtt\":\"15416.744\",\"IP\":\"172.16.100.11\",\"LostNum\":\"-19\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"28000.000\",\"Minrtt\":\"2.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"24\"}','2023-10-25 17:46:30'),(11,'nping_caught','{\"Avgrtt\":\"10832.584\",\"IP\":\"10.100.200.12\",\"LostNum\":\"-12\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"17030.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"17\"}','2023-10-25 17:46:37'),(12,'nping_caught','{\"Avgrtt\":\"8509.559\",\"IP\":\"10.5.0.40\",\"LostNum\":\"-11\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"18009.000\",\"Minrtt\":\"1010.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"16\"}','2023-10-25 17:47:03'),(13,'nping_caught','{\"Avgrtt\":\"4158.056\",\"IP\":\"172.29.40.111\",\"LostNum\":\"-27\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"8000.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"32\"}','2023-10-25 17:47:19'),(14,'nping_caught','{\"Avgrtt\":\"9964.279\",\"IP\":\"172.21.103.144\",\"LostNum\":\"-23\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"22000.000\",\"Minrtt\":\"3000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"28\"}','2023-10-25 17:47:26'),(15,'nping_caught','{\"Avgrtt\":\"5540.130\",\"IP\":\"10.100.200.12\",\"LostNum\":\"-10\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"16040.000\",\"Minrtt\":\"2.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"15\"}','2023-10-25 17:48:07'),(16,'nping_caught','{\"Avgrtt\":\"3848.477\",\"IP\":\"172.29.40.111\",\"LostNum\":\"-28\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"8000.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"33\"}','2023-10-25 17:48:19'),(17,'nping_caught','{\"Avgrtt\":\"8942.746\",\"IP\":\"10.5.0.40\",\"LostNum\":\"-7\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"20046.000\",\"Minrtt\":\"2001.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"12\"}','2023-10-25 17:48:52'),(18,'nping_caught','{\"Avgrtt\":\"3272.750\",\"IP\":\"172.29.40.111\",\"LostNum\":\"-28\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"8000.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"33\"}','2023-10-25 17:49:19'),(19,'nping_caught','{\"Avgrtt\":\"8789.820\",\"IP\":\"172.21.100.73\",\"LostNum\":\"-12\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"18000.000\",\"Minrtt\":\"2000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"17\"}','2023-10-25 17:49:20'),(20,'nping_caught','{\"Avgrtt\":\"96794.574\",\"IP\":\"172.16.102.240\",\"LostNum\":\"-7\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"3469359.200\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"12\"}','2023-10-25 17:49:22'),(21,'nping_caught','{\"Avgrtt\":\"11072.703\",\"IP\":\"10.100.200.12\",\"LostNum\":\"-12\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"17026.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"17\"}','2023-10-25 17:49:37'),(22,'nping_caught','{\"Avgrtt\":\"3753.899\",\"IP\":\"172.29.40.111\",\"LostNum\":\"-27\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"8000.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"32\"}','2023-10-25 17:50:19'),(23,'nping_caught','{\"Avgrtt\":\"214159.882\",\"IP\":\"10.5.0.41\",\"LostNum\":\"-1\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"1235949.300\",\"Minrtt\":\"4000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"6\"}','2023-10-25 17:50:41'),(24,'nping_caught','{\"Avgrtt\":\"5505.871\",\"IP\":\"10.100.200.12\",\"LostNum\":\"-11\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"15023.000\",\"Minrtt\":\"4002.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"16\"}','2023-10-25 17:51:07'),(25,'nping_caught','{\"Avgrtt\":\"9380.995\",\"IP\":\"172.21.103.144\",\"LostNum\":\"-16\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"22000.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"21\"}','2023-10-25 17:51:15'),(26,'nping_caught','{\"Avgrtt\":\"3969.689\",\"IP\":\"172.29.40.111\",\"LostNum\":\"-28\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"8000.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"33\"}','2023-10-25 17:51:19'),(27,'nping_caught','{\"Avgrtt\":\"11831.614\",\"IP\":\"172.16.100.11\",\"LostNum\":\"-16\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"25000.000\",\"Minrtt\":\"2.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"21\"}','2023-10-25 17:52:10'),(28,'nping_caught','{\"Avgrtt\":\"3545.447\",\"IP\":\"172.29.40.111\",\"LostNum\":\"-28\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"8000.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"33\"}','2023-10-25 17:52:19'),(29,'nping_caught','{\"Avgrtt\":\"8482.821\",\"IP\":\"10.5.0.40\",\"LostNum\":\"-12\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"20022.000\",\"Minrtt\":\"1000.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"17\"}','2023-10-25 17:52:31'),(30,'nping_caught','{\"Avgrtt\":\"9656.819\",\"IP\":\"10.100.200.12\",\"LostNum\":\"-12\",\"LostPer\":\"85899345920.00\",\"Maxrtt\":\"16018.000\",\"Minrtt\":\"4007.000\",\"ProbesSent\":\"5\",\"Rcvd\":\"17\"}','2023-10-25 17:52:36');
/*!40000 ALTER TABLE `xnx_logs` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-14  4:27:25
