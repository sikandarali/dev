-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: db-visionpoint.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_collector_version`
--

DROP TABLE IF EXISTS `tbl_collector_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_collector_version` (
  `collector_version_id` int NOT NULL AUTO_INCREMENT,
  `version` varchar(127) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `stamp` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int DEFAULT '0',
  `file_size` varchar(50) DEFAULT NULL,
  `release_notes` longtext,
  `email_notes` longtext,
  `update_schedule` datetime DEFAULT NULL,
  `update_status` enum('OPEN','SCHEDULED','UPDATING','COMPLETED','FAILED') DEFAULT NULL,
  `include_release_notes` tinyint DEFAULT '0',
  PRIMARY KEY (`collector_version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_collector_version`
--

LOCK TABLES `tbl_collector_version` WRITE;
/*!40000 ALTER TABLE `tbl_collector_version` DISABLE KEYS */;
INSERT INTO `tbl_collector_version` VALUES (10,'1.7.8','CXDetect_App.exe','2020-03-25 04:03:59',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(11,'2.0.1','CXDetect_App-2.0.1.exe','2020-06-17 07:14:43',0,'238.10 MB',NULL,NULL,NULL,'COMPLETED',0),(12,'2.0.3','CXDetect_App-2.0.3.exe','2020-06-19 23:39:23',0,'227 MB',NULL,NULL,NULL,'COMPLETED',0),(13,'2.0.4','CXDetect_App-2.0.4.exe','2020-07-01 22:20:58',0,'237.78 MB',NULL,NULL,NULL,'COMPLETED',0),(14,'2.0.5','CXDetect_Bridge-2.0.5.exe','2020-07-30 10:04:58',0,'238.19 MB',NULL,'We will be updating the bridge in order to take advantage of new features. Thank you!','2020-08-06 03:00:00','COMPLETED',1),(15,'2.0.6','CXDetect_Bridge-2.0.6.exe','2020-10-04 16:35:54',0,'64.9 MB',NULL,NULL,NULL,'COMPLETED',0),(16,'2.0.7','CXDetect_Bridge-2.0.7.exe','2020-10-06 09:17:15',0,'64.9 MB',NULL,NULL,NULL,'COMPLETED',0),(17,'2.0.8','CXDetect_Bridge-2.0.8.exe','2020-10-06 09:17:23',0,'64.9 MB',NULL,NULL,'2020-10-07 04:31:00','COMPLETED',0),(18,'2.0.9','CXDetect_Bridge-2.0.9.exe','2020-11-03 16:11:04',0,'63.42 MB',NULL,NULL,NULL,'COMPLETED',0),(19,'2.0.10','CXDetect_Bridge-2.0.10.exe','2020-11-07 03:01:30',0,'63.42 MB','test notes',NULL,'2020-11-19 04:00:00','COMPLETED',0),(20,'2.0.11','CXDetect_Bridge-2.0.11.exe','2021-02-17 08:26:30',0,'63.42 MB','Modification on installer file',NULL,'2021-04-09 17:00:00','COMPLETED',0),(21,'2.0.12','CXDetect_Bridge-2.0.12.exe','2021-04-29 04:06:01',0,'63.42 MB','Fix of Poly VVX device discovery',NULL,'2021-05-11 04:10:00','COMPLETED',0),(22,'2.0.13','CXDetect_Bridge-2.0.13.exe','2021-05-11 10:15:37',0,'63.42 MB','Implementation on Poly x series firmware update',NULL,'2021-05-14 23:20:00','COMPLETED',0),(23,'2.0.14','UnifyPlus_Collector-2.0.14.exe','2021-08-09 15:19:25',0,'63.42 MB','Fix on VVX devices restart issue','Bug Fix: Reboot API updated for the following models.\n\nPhone Model VVX 311 UC Software Version	5.9.0.9373 (Vonage)\nPhone Model VVX 411 UC Software Version 5.9.1.0615 (Vonage)','2021-10-16 05:59:00','COMPLETED',0),(24,'2.0.15','UnifyPlus_Collector-2.0.15.exe','2022-03-31 07:43:26',0,'61.9 MB','Fixed special character in password error',NULL,'2022-04-11 19:45:00','COMPLETED',0),(25,'2.1.0','UnifyPlus_Collector-2.1.0.exe','2022-08-25 00:00:00',0,'62.6 MB','Simplified logging',NULL,NULL,'COMPLETED',0),(26,'2.1.8','UnifyPlus_Collector-2.1.8.exe','2023-03-28 00:00:00',0,'99 MB','IoT feature added.',NULL,'2023-05-09 14:30:00','COMPLETED',0),(27,'2.1.11','UnifyPlus_Collector-2.1.11.exe','2023-05-19 00:00:00',0,'99 MB','IoT feature added.',NULL,'2023-05-09 14:30:00','COMPLETED',0),(28,'2.1.15','UnifyPlus_Collector-2.1.15.exe','2023-10-23 00:00:00',1,'99.3 MB','Poly 4.1.X support',NULL,'2023-10-25 16:25:00','UPDATING',0);
/*!40000 ALTER TABLE `tbl_collector_version` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-14  3:37:50
