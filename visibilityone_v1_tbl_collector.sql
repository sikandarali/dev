-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: db-visionpoint.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_collector`
--

DROP TABLE IF EXISTS `tbl_collector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_collector` (
  `collector_id` int unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int unsigned DEFAULT NULL,
  `company_id` int unsigned DEFAULT NULL,
  `collector_name` varchar(45) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `mac_address` varchar(45) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `host_name` varchar(127) DEFAULT NULL,
  `network_type` varchar(45) DEFAULT NULL,
  `verified` int DEFAULT '0',
  `paused` int DEFAULT '0',
  `pair_code` int DEFAULT NULL,
  `alerts` int DEFAULT '1',
  `background_qos` int DEFAULT '1',
  `active` int DEFAULT '0',
  `status` int DEFAULT '-1',
  `create_date` datetime DEFAULT NULL,
  `verify_date` datetime DEFAULT NULL,
  `last_connect` datetime DEFAULT NULL,
  `last_message` varchar(255) DEFAULT NULL,
  `stats_cpu_usage` float DEFAULT NULL,
  `stats_mem_free` bigint DEFAULT NULL,
  `stats_mem_total` bigint DEFAULT NULL,
  `stats_hd_details` text,
  `max_threads` int DEFAULT '20',
  `request_interval` int DEFAULT '30',
  `ping_interval` int DEFAULT '10',
  `background_qos_interval` int DEFAULT '300',
  `version` varchar(45) DEFAULT NULL,
  `logging` int DEFAULT '0',
  `collector_update_schedule` datetime DEFAULT NULL,
  `scheduled_update_setting` tinyint DEFAULT '0',
  `local_utc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`collector_id`),
  KEY `site_collector_idx` (`site_id`),
  KEY `collector_company_idx` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_collector`
--

LOCK TABLES `tbl_collector` WRITE;
/*!40000 ALTER TABLE `tbl_collector` DISABLE KEYS */;
INSERT INTO `tbl_collector` VALUES (199,NULL,NULL,NULL,NULL,'1C:39:47:E5:BF:AD','192.168.0.204','ERTSYOGA510','Ethernet',0,0,630521,1,1,0,1,'2022-03-07 00:23:56',NULL,'2022-03-10 23:11:19',NULL,52,3725750272,22920835072,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":542292758528,\\\"total\\\":999218249728}]\"',20,30,10,300,'2.0.14',0,NULL,0,NULL),(200,102,36,NULL,NULL,'94:C6:91:0F:11:47','172.19.3.108','DESKTOP-BD02M9C','Ethernet',0,0,171424,1,1,0,1,'2022-03-08 23:33:14','2022-05-27 04:38:24','2023-03-28 08:25:45',NULL,15,4326854656,19632750592,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":175776493568,\\\"total\\\":213738500096},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":776392110080,\\\"total\\\":785382371328},{\\\"drive\\\":\\\"e:/\\\",\\\"free\\\":-1,\\\"total\\\":-1}]\"',20,30,10,300,'2.1.0',0,NULL,0,NULL),(202,94,36,NULL,NULL,'14:7d:da:8e:ce:a4','172.19.3.141','Frankies-Mini.socal.rr.com',NULL,0,0,387078,1,1,0,-1,'2022-03-11 06:33:53','2022-03-11 06:37:55','2022-03-22 08:09:43',NULL,0.00672873,25616384,17179869184,'\"[{\\\"free\\\":25616384,\\\"total\\\":17179869184}]\"',20,30,10,300,'2.0.2_mac',0,NULL,0,NULL),(203,111,395,NULL,NULL,'00:15:5D:64:92:2A','172.21.102.241','Unify-Plus-VP','Ethernet',1,0,724768,1,1,1,1,'2022-03-15 19:06:04','2023-03-28 19:14:08','2024-02-12 19:46:48',NULL,2,10850578432,14279045120,'\"[{\\\"drive\\\":\\\"a:/\\\",\\\"free\\\":-1,\\\"total\\\":-1},{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":212587982848,\\\"total\\\":267849359360},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":0,\\\"total\\\":5268953088}]\"',20,30,10,300,'2.1.8',0,'2023-10-25 16:25:00',0,'-05:00'),(204,NULL,NULL,NULL,NULL,'14:7d:da:8e:ce:a4','172.19.3.141','Frankies-Mini.socal.rr.com',NULL,0,0,902894,1,1,0,1,'2022-06-02 03:25:00',NULL,'2022-06-02 03:25:20',NULL,0.00867047,5130342400,17179869184,'\"[{\\\"free\\\":5130342400,\\\"total\\\":17179869184}]\"',20,30,10,300,'2.0.2_mac',0,NULL,0,NULL),(205,NULL,NULL,NULL,NULL,'3c:06:30:4f:c1:b1','192.168.254.109',NULL,NULL,0,0,194962,1,1,0,-1,'2022-07-27 05:09:30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,30,10,300,NULL,0,NULL,0,NULL),(206,NULL,NULL,NULL,NULL,'38:F3:AB:3D:FA:5D','192.168.0.102','PURE-DEV-JUNAID','Ethernet',0,0,194525,1,1,0,1,'2022-07-29 02:59:20',NULL,'2022-07-29 05:41:26',NULL,23,12133625856,19661684736,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":17516920832,\\\"total\\\":214213644288},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":203599233024,\\\"total\\\":285236785152}]\"',20,30,10,300,'2.1.0',0,NULL,0,NULL),(207,NULL,NULL,NULL,NULL,'38:F3:AB:3D:FA:5D','192.168.0.110','PURE-DEV-JUNAID','Ethernet',0,0,886046,1,1,0,1,'2022-07-30 21:33:25',NULL,'2022-07-30 21:33:51',NULL,35,11359248384,19527467008,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":19810922496,\\\"total\\\":214213644288},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":203109330944,\\\"total\\\":285236785152}]\"',20,30,10,300,'2.1.0',0,NULL,0,NULL),(208,116,36,NULL,NULL,'60:18:95:76:10:60','192.168.1.232','V1-Jonathan','Ethernet',0,0,775965,1,1,0,1,'2022-08-03 00:21:42','2023-05-15 21:08:08','2023-05-16 00:30:18',NULL,0,13683658752,29368782848,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":322526334976,\\\"total\\\":490766069760}]\"',20,30,10,300,'2.1.8',0,NULL,0,'UTC-04:00'),(209,NULL,NULL,NULL,NULL,'14:58:D0:B9:25:A5','192.168.1.6','DESKTOP-OMHL5SK','Unknown',0,0,725442,1,1,0,1,'2022-10-10 11:40:28',NULL,'2023-10-23 19:27:09',NULL,27,15801180160,28311072768,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":5040394240,\\\"total\\\":127381204992},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":91440168960,\\\"total\\\":499497562112}]\"',20,30,10,300,'2.1.15',0,NULL,0,' 05:00'),(210,104,405,NULL,NULL,'88:AE:DD:09:5D:EA','172.17.108.53','DESKTOP-DVG6I1K','Ethernet',1,0,152782,1,1,1,-1,'2022-10-28 17:50:24','2022-10-28 18:03:29','2023-10-31 12:17:21',NULL,28,5693005824,8921948160,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":222245519360,\\\"total\\\":255269531648}]\"',20,30,10,300,'2.1.0',0,NULL,0,'-04:00'),(211,NULL,NULL,NULL,NULL,'38:D5:47:E1:32:51','172.21.101.192','VP-Program-005','Ethernet',0,0,760292,1,1,0,1,'2023-03-22 19:04:08',NULL,'2023-03-22 19:09:30',NULL,74,25694806016,39380004864,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":751998091264,\\\"total\\\":999429238784},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":1000048300032,\\\"total\\\":1000186310656},{\\\"drive\\\":\\\"e:/\\\",\\\"free\\\":3247601418240,\\\"total\\\":4000768323584},{\\\"drive\\\":\\\"f:/\\\",\\\"free\\\":3981729792,\\\"total\\\":4018126848}]\"',20,30,10,300,'2.1.0',0,NULL,0,NULL),(212,NULL,NULL,NULL,NULL,'38:D5:47:E1:32:51','172.21.101.192','VP-Program-005','Ethernet',0,0,919069,1,1,0,1,'2023-03-22 19:09:42',NULL,'2023-03-22 19:14:45',NULL,7,23767355392,39380004864,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":752032034816,\\\"total\\\":999429238784},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":1000048300032,\\\"total\\\":1000186310656},{\\\"drive\\\":\\\"e:/\\\",\\\"free\\\":3247601418240,\\\"total\\\":4000768323584},{\\\"drive\\\":\\\"f:/\\\",\\\"free\\\":3981729792,\\\"total\\\":4018126848}]\"',20,30,10,300,'2.1.0',0,NULL,0,NULL),(213,NULL,NULL,NULL,NULL,'38:D5:47:E1:32:51','172.21.101.192','VP-Program-005','Ethernet',0,0,516337,1,1,0,1,'2023-03-22 19:14:50',NULL,'2024-02-12 19:46:53',NULL,17,23065219072,39380004864,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":689230475264,\\\"total\\\":999429238784},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":965906206720,\\\"total\\\":1000186310656},{\\\"drive\\\":\\\"e:/\\\",\\\"free\\\":2977961684992,\\\"total\\\":4000768323584},{\\\"drive\\\":\\\"f:/\\\",\\\"free\\\":3978719232,\\\"total\\\":4018126848}]\"',20,30,10,300,'2.1.0',0,NULL,0,'-05:00'),(214,106,415,NULL,NULL,'00:15:5D:54:1D:DA','172.17.100.69','DESKTOP-9FR1HSM','Ethernet',0,0,849028,1,1,0,1,'2023-03-23 14:31:18','2023-03-23 14:49:46','2023-03-23 15:20:19',NULL,1,3776061440,10244272128,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":192327811072,\\\"total\\\":254721126400}]\"',20,30,10,300,'2.1.0',0,NULL,0,NULL),(215,108,415,NULL,NULL,'84:A9:38:B2:95:D1','172.29.224.1','DESKTOP-H4PHOVN','Ethernet',0,0,357071,1,1,0,1,'2023-03-24 16:15:07','2023-03-24 16:20:18','2023-12-04 16:46:11',NULL,5,2626199552,9506074624,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":196508712960,\\\"total\\\":253518729216}]\"',20,30,10,300,'2.1.0',0,NULL,0,'-05:00'),(216,109,419,NULL,NULL,'9C:2D:CD:20:12:37','172.21.101.201','Kleinberg-RemotePC-UnifyPlus','Ethernet',1,0,797966,1,1,1,1,'2023-03-27 19:16:59','2023-03-27 19:28:01','2024-02-12 19:46:53',NULL,0,8577716224,13893115904,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":193905901568,\\\"total\\\":253672550400}]\"',20,30,10,300,'2.1.15',0,'2023-10-25 16:25:00',0,'-05:00'),(218,118,36,NULL,NULL,'94:C6:91:0F:99:D3','172.19.3.113','visibilityONE-PC','Ethernet',1,0,575913,1,1,1,-1,'2023-04-21 06:56:56','2023-05-29 17:59:07','2023-06-02 17:45:23',NULL,19,2809991168,9834868736,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":864718061568,\\\"total\\\":999609593856},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":-1,\\\"total\\\":-1}]\"',20,30,10,300,'2.1.8',0,'2023-10-25 16:25:00',0,'UTC-07:00'),(219,126,398,NULL,NULL,'60:18:95:76:10:60','192.168.1.80','V1-Jonathan','Ethernet',1,0,737243,1,1,1,-1,'2023-05-16 00:30:23','2023-11-02 19:01:37','2023-11-06 17:26:47',NULL,4,19505278976,28295041024,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":299243524096,\\\"total\\\":490766069760}]\"',20,30,10,300,'2.1.15',0,NULL,0,'-05:00'),(220,NULL,NULL,NULL,NULL,'A4:17:91:22:4A:F8','10.9.155.12','DESKTOP-0669I8M','Ethernet',0,0,492656,1,1,0,1,'2023-08-04 13:28:56',NULL,'2023-10-24 13:07:54',NULL,0,5040308224,9719984128,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":82536017920,\\\"total\\\":126847283200}]\"',20,30,10,300,'2.1.11',0,NULL,0,'-04:00'),(221,NULL,NULL,NULL,NULL,'A4:17:91:27:81:3D','10.9.155.21','DESKTOP-5P9GGA5','Ethernet',0,0,893009,1,1,0,1,'2023-08-04 17:58:56',NULL,'2023-08-04 18:12:55',NULL,34,4416344064,9726910464,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":51827310592,\\\"total\\\":127719698432}]\"',20,30,10,300,'2.1.11',0,NULL,0,'UTC-04:00'),(222,121,421,NULL,NULL,'E8:80:88:B4:5B:F4','172.16.100.170','Unify-ProdVentu','Ethernet',1,0,615725,1,1,1,1,'2023-08-21 14:08:47','2023-08-21 14:10:03','2024-01-04 15:13:34',NULL,7,8306155520,9463926784,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":198414905344,\\\"total\\\":253672550400}]\"',20,30,10,300,'2.1.15',0,'2023-10-25 16:25:00',0,'-05:00'),(223,122,415,NULL,NULL,'E8:80:88:AD:0E:FE','10.5.0.70','P1318-collector','Ethernet',1,0,861914,1,1,1,1,'2023-09-19 15:32:39','2023-09-19 15:59:22','2024-02-12 19:46:53',NULL,1,26267549696,30005895168,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":168924127232,\\\"total\\\":253672550400}]\"',20,30,10,300,'2.1.15',0,'2023-10-25 16:25:00',0,'-05:00'),(224,123,416,NULL,NULL,'E8:80:88:6A:A1:EA','172.17.100.242','P1313-DP-Collector','Ethernet',1,0,334697,1,1,1,1,'2023-09-21 15:02:46','2023-09-21 16:21:58','2024-02-12 19:46:53',NULL,4,10220056576,19395678208,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":451928166400,\\\"total\\\":509722226688}]\"',20,30,10,300,'2.1.15',0,'2023-10-25 16:25:00',0,'-08:00'),(225,124,417,NULL,NULL,'9C:2D:CD:1F:FD:BA','172.25.21.3','P1288-CS-Collec','Ethernet',1,0,927810,1,1,1,1,'2023-10-09 23:16:22','2023-10-09 23:17:51','2024-02-12 19:46:53',NULL,1,7239667712,14698418176,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":192530370560,\\\"total\\\":253672550400}]\"',20,30,10,300,'2.1.15',0,'2023-10-25 16:25:00',0,'-05:00');
/*!40000 ALTER TABLE `tbl_collector` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-13  0:47:07
