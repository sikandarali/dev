-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: db-visionpoint.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_audio_device`
--

DROP TABLE IF EXISTS `tbl_audio_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_audio_device` (
  `audio_device_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int unsigned DEFAULT NULL,
  `audio_device_catalog_id` bigint unsigned DEFAULT NULL,
  `company_id` int unsigned DEFAULT NULL,
  `collector_id` int unsigned DEFAULT NULL,
  `room_name` varchar(45) DEFAULT NULL,
  `device_type` varchar(45) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `serial_number` varchar(45) DEFAULT NULL,
  `duplex` varchar(45) DEFAULT NULL,
  `ipv4` varchar(45) DEFAULT NULL,
  `mac` varchar(127) DEFAULT NULL,
  `uptime` json DEFAULT NULL,
  `username` varchar(127) DEFAULT NULL,
  `password` varchar(127) DEFAULT NULL,
  `notes` text,
  `health` int DEFAULT NULL,
  `qos` int DEFAULT NULL,
  `paused` int DEFAULT '0',
  `presence` varchar(45) DEFAULT NULL,
  `create_stamp` datetime DEFAULT NULL,
  `sync_stamp` datetime DEFAULT NULL,
  `on_call` int DEFAULT NULL,
  `online` int DEFAULT '1',
  `active` int DEFAULT '1',
  `state` varchar(45) DEFAULT NULL,
  `state_data` varchar(127) DEFAULT NULL,
  `health_info` json DEFAULT NULL,
  `triggers` json DEFAULT NULL,
  `sip_info` json DEFAULT NULL,
  `running_config` json DEFAULT NULL,
  `network_info` json DEFAULT NULL,
  `firmware` varchar(45) DEFAULT NULL,
  `mic_muted` varchar(45) DEFAULT NULL,
  `last_call_id` bigint DEFAULT '-1',
  `network_connection_type` varchar(45) DEFAULT NULL,
  `network_connection_status` varchar(45) DEFAULT NULL,
  `bluetooth_signal` varchar(45) DEFAULT NULL,
  `mircrosoft_service` varchar(45) DEFAULT NULL,
  `microsoft_team_service` int DEFAULT NULL,
  `zoom_service` int DEFAULT NULL,
  `webex_service` int DEFAULT NULL,
  `direct_access_token` varchar(127) DEFAULT NULL,
  `direct_access_info` json DEFAULT NULL,
  `mr_pair` json DEFAULT NULL,
  PRIMARY KEY (`audio_device_id`),
  KEY `site_idx` (`site_id`),
  KEY `device_catalog_idx` (`audio_device_catalog_id`),
  KEY `audio_device_collector_idx` (`collector_id`),
  KEY `report_idx` (`company_id`,`site_id`,`active`,`collector_id`,`device_type`),
  CONSTRAINT `audio_device_catalog` FOREIGN KEY (`audio_device_catalog_id`) REFERENCES `tbl_audio_device_catalog` (`audio_device_catalog_id`),
  CONSTRAINT `audio_device_collector` FOREIGN KEY (`collector_id`) REFERENCES `tbl_collector` (`collector_id`),
  CONSTRAINT `site_audiodevice` FOREIGN KEY (`site_id`) REFERENCES `tbl_site` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_audio_device`
--

LOCK TABLES `tbl_audio_device` WRITE;
/*!40000 ALTER TABLE `tbl_audio_device` DISABLE KEYS */;
INSERT INTO `tbl_audio_device` VALUES (18,97,NULL,395,203,'Training Room Phone','vvx','VVX 411',NULL,NULL,'172.21.100.162','64167f64f8ca','{\"hrs\": \"0\", \"days\": \"0\", \"mins\": \":\"}','Polycom','4369673',NULL,0,0,0,'','2022-03-25 15:06:20','2022-05-19 15:28:03',0,0,0,'Error','All Phone applications are not ready.','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}',NULL,'{\"user\": \"61259059404504099270\", \"server\": \"\", \"status\": \"\"}','{\"sntp_server\": \"pool.ntp.org\", \"gmt_offset_hours\": \"-5\", \"dns_primary_server\": \"172.21.103.25\", \"provisioning_server\": \"https://provpp.zoom.us/api/v2/pbx/provisioning/Polycom/vvx411\", \"dns_secondary_server\": \"172.21.103.26\", \"provisioning_server_type\": \"HTTPS\"}','{\"dhcp\": \"enabled\", \"wifi\": {}, \"signal\": {\"bluetooth\": {\"devices\": [], \"enabled\": \"0\", \"version\": \"\"}}, \"vlan_id\": \"\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"172.21.103.25\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','6.4.2.3176','false',943,NULL,NULL,NULL,NULL,1,1,0,NULL,NULL,NULL),(19,97,NULL,395,203,'QC Desk','vvx','VVX 411',NULL,NULL,'172.17.100.55','64167f64f528','{\"hrs\": \"1\", \"days\": \"4\", \"mins\": \"3\"}','Polycom','4369673',NULL,0,0,0,'','2022-04-11 17:30:19','2023-03-04 23:53:19',0,0,0,'Idle','Time of last call 2023-02-28T16:04:57','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}',NULL,'{\"user\": \"60841215863857547523\", \"server\": \"\", \"status\": \"Registered\"}','{\"sntp_server\": \"172.17.100.1\", \"gmt_offset_hours\": \"-5\", \"dns_primary_server\": \"208.67.222.222\", \"provisioning_server\": \"https://provpp.zoom.us/api/v2/pbx/provisioning/Polycom/vvx411\", \"dns_secondary_server\": \"208.67.220.220\", \"provisioning_server_type\": \"HTTPS\"}','{\"dhcp\": \"enabled\", \"wifi\": {}, \"signal\": {\"bluetooth\": {\"devices\": [], \"enabled\": \"0\", \"version\": \"\"}}, \"vlan_id\": \"\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"172.17.100.1\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','6.4.4.3178','false',4379,NULL,NULL,NULL,NULL,1,1,0,NULL,NULL,NULL),(20,97,NULL,395,203,'Rack Fabrication - Roberto','vvx','VVX 411',NULL,NULL,'172.17.101.150','64167f64848b','{\"hrs\": \"1\", \"days\": \"0\", \"mins\": \"7\"}','Polycom','4369673',NULL,0,0,0,'','2022-04-11 17:33:17','2023-02-08 20:03:36',0,0,0,'Idle','Time of last call 2023-02-07T12:57:55','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}',NULL,'{\"user\": \"22614904350373826759\", \"server\": \"\", \"status\": \"Registered\"}','{\"sntp_server\": \"172.17.100.1\", \"gmt_offset_hours\": \"-5\", \"dns_primary_server\": \"208.67.222.222\", \"provisioning_server\": \"https://provpp.zoom.us/api/v2/pbx/provisioning/Polycom/vvx411\", \"dns_secondary_server\": \"208.67.220.220\", \"provisioning_server_type\": \"HTTPS\"}','{\"dhcp\": \"enabled\", \"wifi\": {}, \"signal\": {\"bluetooth\": {\"devices\": [], \"enabled\": \"0\", \"version\": \"\"}}, \"vlan_id\": \"\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"172.17.100.1\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','6.4.3.5156','false',4239,NULL,NULL,NULL,NULL,1,1,0,NULL,NULL,NULL),(21,97,NULL,395,203,'Rack Fabrication - Sheldon','vvx','VVX 411',NULL,NULL,'172.17.100.53','64167f649793','{\"hrs\": \"1\", \"days\": \"0\", \"mins\": \":\"}','Polycom','4369673',NULL,0,0,0,'','2022-04-11 17:34:47','2023-02-08 20:03:36',0,0,0,'Idle','Time of last call 2023-02-08T13:56:07','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}',NULL,'{\"user\": \"33535081004676665853\", \"server\": \"\", \"status\": \"Registered\"}','{\"sntp_server\": \"172.17.100.1\", \"gmt_offset_hours\": \"-5\", \"dns_primary_server\": \"208.67.222.222\", \"provisioning_server\": \"https://provpp.zoom.us/api/v2/pbx/provisioning/Polycom/vvx411\", \"dns_secondary_server\": \"208.67.220.220\", \"provisioning_server_type\": \"HTTPS\"}','{\"dhcp\": \"enabled\", \"wifi\": {}, \"signal\": {\"bluetooth\": {\"devices\": [], \"enabled\": \"0\", \"version\": \"\"}}, \"vlan_id\": \"\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"172.17.100.1\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','6.4.3.5156','false',4236,NULL,NULL,NULL,NULL,1,1,0,NULL,NULL,NULL),(22,97,NULL,395,203,'Kitchen','vvx','VVX 411',NULL,NULL,'172.24.100.18','64167f649e66','{\"hrs\": \"8\", \"days\": \"28\", \"mins\": \":\"}','Polycom','4369673',NULL,100,0,0,'','2022-04-11 17:42:01','2023-03-28 19:01:50',0,1,1,'Idle','Time of last call 2023-03-24T17:24:40','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}',NULL,'{\"user\": \"47804450510863069508\", \"server\": \"\", \"status\": \"Registered\"}','{\"sntp_server\": \"172.24.100.1\", \"gmt_offset_hours\": \"-5\", \"dns_primary_server\": \"172.24.100.1\", \"provisioning_server\": \"https://provpp.zoom.us/api/v2/pbx/provisioning/Polycom/vvx411\", \"dns_secondary_server\": \"0.0.0.0\", \"provisioning_server_type\": \"HTTPS\"}','{\"dhcp\": \"enabled\", \"wifi\": {}, \"signal\": {\"bluetooth\": {\"devices\": [], \"enabled\": \"0\", \"version\": \"\"}}, \"vlan_id\": \"20\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"172.24.100.1\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','6.4.4.3178','false',4382,NULL,NULL,NULL,NULL,1,1,0,NULL,NULL,NULL),(27,112,NULL,395,203,'Alex\'s Phone','vvx','VVX 411',NULL,NULL,'172.21.101.27','64167f465877','{\"hrs\": \"2\", \"days\": \"0\", \"mins\": \"1\"}','Polycom','4369673',NULL,0,0,0,'','2023-04-03 19:13:12','2023-08-29 14:01:50',0,0,1,'Idle','Time of last call 2023-08-29T08:26:47','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}',NULL,'{\"user\": \"32533566008939362261\", \"server\": \"\", \"status\": \"Registered\"}','{\"sntp_server\": \"pool.ntp.org\", \"gmt_offset_hours\": \"-5\", \"dns_primary_server\": \"172.21.103.25\", \"provisioning_server\": \"https://provpp.zoom.us/api/v2/pbx/provisioning/Polycom/vvx411\", \"dns_secondary_server\": \"172.21.103.26\", \"provisioning_server_type\": \"HTTPS\"}','{\"dhcp\": \"enabled\", \"wifi\": {}, \"signal\": {\"bluetooth\": {\"devices\": [], \"enabled\": \"0\", \"version\": \"\"}}, \"vlan_id\": \"\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"172.21.103.25\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','6.4.5.1210','false',7485,NULL,NULL,NULL,NULL,1,1,0,NULL,NULL,NULL),(28,112,NULL,395,203,'Bryan\'s Phone','vvx','VVX 411',NULL,NULL,'172.21.101.22','64167f9c4611','{\"hrs\": \"1\", \"days\": \"28\", \"mins\": \"3\"}','Polycom','4369673',NULL,100,0,0,'','2023-04-03 20:54:17','2024-02-12 19:45:40',0,1,1,'Idle','Time of last call 2023-12-11T14:00:15','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}',NULL,'{\"user\": \"57319336139555387183\", \"server\": \"\", \"status\": \"\"}','{\"sntp_server\": \"pool.ntp.org\", \"gmt_offset_hours\": \"-5\", \"dns_primary_server\": \"172.21.103.25\", \"provisioning_server\": \"https://provpp.zoom.us/api/v2/pbx/provisioning/Polycom/vvx411\", \"dns_secondary_server\": \"172.21.103.26\", \"provisioning_server_type\": \"HTTPS\"}','{\"dhcp\": \"enabled\", \"wifi\": {}, \"signal\": {\"bluetooth\": {\"devices\": [], \"enabled\": \"0\", \"version\": \"\"}}, \"vlan_id\": \"\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"172.21.103.25\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','6.4.6.2453','false',9288,NULL,NULL,NULL,NULL,1,1,0,NULL,NULL,NULL),(29,112,NULL,395,203,'Roberto\'s Phone','vvx','VVX 411',NULL,NULL,'172.21.100.221','64167f27e9f0','{\"hrs\": \"1\", \"days\": \"27\", \"mins\": \"0\"}','Polycom','4369673',NULL,100,0,0,'','2023-04-03 20:57:09','2024-02-12 19:45:40',0,1,1,'Idle','Time of last call 2024-01-31T11:12:35','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}',NULL,'{\"user\": \"27107160173812785242\", \"server\": \"\", \"status\": \"Registered\"}','{\"sntp_server\": \"pool.ntp.org\", \"gmt_offset_hours\": \"-5\", \"dns_primary_server\": \"172.21.103.25\", \"provisioning_server\": \"https://provpp.zoom.us/api/v2/pbx/provisioning/Polycom/vvx411\", \"dns_secondary_server\": \"172.21.103.26\", \"provisioning_server_type\": \"HTTPS\"}','{\"dhcp\": \"enabled\", \"wifi\": {}, \"signal\": {\"bluetooth\": {\"devices\": [], \"enabled\": \"0\", \"version\": \"\"}}, \"vlan_id\": \"\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"172.21.103.25\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','6.4.6.2453','false',9511,NULL,NULL,NULL,NULL,1,1,0,NULL,NULL,NULL),(31,115,NULL,36,218,'trio','trioDevice','Trio 8800',NULL,NULL,'172.19.3.111','0004f2fcbcb4','{\"hrs\": \"1\", \"days\": \"19\", \"mins\": \"1\"}','Polycom','9976',NULL,0,NULL,0,'','2023-04-21 07:34:48','2023-05-22 21:45:19',0,0,0,'Idle','',NULL,NULL,'{\"user\": \"Trio8800\", \"server\": \"\", \"status\": \"\"}','{\"sntp_server\": \"time.windows.com\", \"gmt_offset_hours\": \"-8\", \"dns_primary_server\": \"172.19.3.254\", \"provisioning_server\": \"\", \"dns_secondary_server\": \"\", \"provisioning_server_type\": \"FTP\"}','{\"dhcp\": \"disabled\", \"wifi\": {}, \"signal\": {}, \"vlan_id\": \"\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"0.0.0.0\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','7.2.1.1679','false',-1,NULL,NULL,NULL,NULL,1,1,1,NULL,NULL,NULL),(32,113,NULL,395,203,'Lonny\'s Phone','vvx','VVX 411',NULL,NULL,'172.24.100.26','64167f647734','{\"hrs\": \"1\", \"days\": \"28\", \"mins\": \"8\"}','Polycom','4369673',NULL,100,0,0,'','2023-04-24 18:27:37','2024-02-12 19:45:40',0,1,1,'Idle','Time of last call 2023-06-08T09:44:23','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}',NULL,'{\"user\": \"22036587300508574358\", \"server\": \"\", \"status\": \"Registered\"}','{\"sntp_server\": \"172.24.100.1\", \"gmt_offset_hours\": \"0\", \"dns_primary_server\": \"172.24.100.1\", \"provisioning_server\": \"https://provpp.zoom.us/api/v2/pbx/provisioning/Polycom/vvx411\", \"dns_secondary_server\": \"0.0.0.0\", \"provisioning_server_type\": \"HTTPS\"}','{\"dhcp\": \"enabled\", \"wifi\": {}, \"signal\": {\"bluetooth\": {\"devices\": [], \"enabled\": \"0\", \"version\": \"\"}}, \"vlan_id\": \"20\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"172.24.100.1\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','6.4.6.2453','false',6182,NULL,NULL,NULL,NULL,1,1,0,NULL,NULL,NULL),(33,113,NULL,395,203,'David J\'s Phone','vvx','VVX 411',NULL,NULL,'172.24.100.12','64167f64fd88','{\"hrs\": \"9\", \"days\": \"27\", \"mins\": \":\"}','Polycom','4369673',NULL,100,0,0,'','2023-04-24 18:29:00','2024-02-12 19:45:40',0,1,1,'Idle','Time of last call 2024-02-08T08:31:57','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}',NULL,'{\"user\": \"73411186576337358719\", \"server\": \"\", \"status\": \"Registered\"}','{\"sntp_server\": \"172.24.100.1\", \"gmt_offset_hours\": \"-5\", \"dns_primary_server\": \"172.24.100.1\", \"provisioning_server\": \"https://provpp.zoom.us/api/v2/pbx/provisioning/Polycom/vvx411\", \"dns_secondary_server\": \"0.0.0.0\", \"provisioning_server_type\": \"HTTPS\"}','{\"dhcp\": \"enabled\", \"wifi\": {}, \"signal\": {\"bluetooth\": {\"devices\": [], \"enabled\": \"0\", \"version\": \"\"}}, \"vlan_id\": \"20\", \"lan_speed\": \"100Mbps\", \"dhcp_server\": \"172.24.100.1\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','6.4.6.2453','false',9510,NULL,NULL,NULL,NULL,1,1,0,NULL,NULL,NULL),(34,113,NULL,395,203,'Mallory\'s Phone','vvx','VVX 411',NULL,NULL,'172.24.100.28','64167f64a2e3','{\"hrs\": \"9\", \"days\": \"27\", \"mins\": \":\"}','Polycom','4369673',NULL,100,0,0,'','2023-04-24 18:31:00','2024-02-12 19:45:40',0,1,1,'Idle','Time of last call 2024-02-12T13:33:30','{\"qos\": 1, \"health\": 100.0, \"healthPoint\": 4.0, \"healthTrigger\": 0.0, \"notificationTriggers\": 0}',NULL,'{\"user\": \"51370088788874635566\", \"server\": \"\", \"status\": \"Registered\"}','{\"sntp_server\": \"172.24.100.1\", \"gmt_offset_hours\": \"-5\", \"dns_primary_server\": \"172.24.100.1\", \"provisioning_server\": \"https://provpp.zoom.us/api/v2/pbx/provisioning/Polycom/vvx411\", \"dns_secondary_server\": \"0.0.0.0\", \"provisioning_server_type\": \"HTTPS\"}','{\"dhcp\": \"enabled\", \"wifi\": {}, \"signal\": {\"bluetooth\": {\"devices\": [], \"enabled\": \"0\", \"version\": \"\"}}, \"vlan_id\": \"20\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"172.24.100.1\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','6.4.6.2453','false',9512,NULL,NULL,NULL,NULL,1,1,0,NULL,NULL,NULL),(35,118,NULL,36,218,'poly','trioDevice','Trio 8800',NULL,NULL,'172.19.3.111','0004f2fcbcb4','{\"hrs\": \"2\", \"days\": \"3\", \"mins\": \"2\"}','Polycom','9976',NULL,100,NULL,0,'','2023-06-02 00:29:11','2023-06-02 17:45:24',0,1,1,'Idle','',NULL,NULL,'{\"user\": \"790078258\", \"server\": \"\", \"status\": \"\"}','{\"sntp_server\": \"time.windows.com\", \"gmt_offset_hours\": \"-8\", \"dns_primary_server\": \"172.19.3.254\", \"provisioning_server\": \"\", \"dns_secondary_server\": \"\", \"provisioning_server_type\": \"FTP\"}','{\"dhcp\": \"disabled\", \"wifi\": {}, \"signal\": {}, \"vlan_id\": \"\", \"lan_speed\": \"1000Mbps\", \"dhcp_server\": \"0.0.0.0\", \"vlan_id_option\": \"129\", \"lan_port_status\": \"active\"}','7.2.1.1679','false',-1,NULL,NULL,NULL,NULL,1,1,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_audio_device` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-13  0:47:19
