-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: db-visionpoint.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Temporary view structure for view `v_video_data`
--

DROP TABLE IF EXISTS `v_video_data`;
/*!50001 DROP VIEW IF EXISTS `v_video_data`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_video_data` AS SELECT 
 1 AS `video_data_id`,
 1 AS `video_device_id`,
 1 AS `company_id`,
 1 AS `site_id`,
 1 AS `video_call_id`,
 1 AS `timestamp`,
 1 AS `total_mbps`,
 1 AS `audioRx_mbps`,
 1 AS `audioTx_mbps`,
 1 AS `videoRx_mbps`,
 1 AS `videoTx_mbps`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_video_data`
--

/*!50001 DROP VIEW IF EXISTS `v_video_data`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`vsone`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_video_data` AS select `tbl_video_data`.`video_data_id` AS `video_data_id`,`tbl_video_data`.`video_device_id` AS `video_device_id`,`tbl_video_data`.`company_id` AS `company_id`,`tbl_video_data`.`site_id` AS `site_id`,`tbl_video_data`.`video_call_id` AS `video_call_id`,`tbl_video_data`.`timestamp` AS `timestamp`,`tbl_video_data`.`total_mbps` AS `total_mbps`,json_extract(`tbl_video_data`.`audioRx`,'$.mbps') AS `audioRx_mbps`,json_extract(`tbl_video_data`.`audioTx`,'$.mbps') AS `audioTx_mbps`,json_extract(`tbl_video_data`.`videoRx`,'$.mbps') AS `videoRx_mbps`,json_extract(`tbl_video_data`.`videoTx`,'$.mbps') AS `videoTx_mbps` from `tbl_video_data` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-14  4:30:36
